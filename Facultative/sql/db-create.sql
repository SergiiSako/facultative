DROP DATABASE IF EXISTS facultative;
CREATE DATABASE facultative DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use facultative;
CREATE TABLE roles
(
    id_role   INT PRIMARY KEY AUTO_INCREMENT,
    name_role VARCHAR(10) NOT NULL UNIQUE
);
INSERT INTO roles VALUES
       (1, 'ADMIN'),
       (2, 'STUDENT'),
       (3, 'TEACHER'),
       (4, 'GUEST');

CREATE TABLE state
(
    id_cond   INT PRIMARY KEY AUTO_INCREMENT,
    name_cond VARCHAR(10) NOT NULL UNIQUE
);
INSERT INTO state VALUES
       (1, 'ACTIVE'),
       (2, 'BLOCKED');

CREATE TABLE THEMES
(
    id_theme   INT PRIMARY KEY AUTO_INCREMENT,
    name_theme VARCHAR(30) NOT NULL UNIQUE
);

INSERT INTO THEMES
VALUES (DEFAULT, 'WEB Course'),
       (DEFAULT, 'PROGRAMMING'),
       (DEFAULT, 'GameDev'),
       (DEFAULT, 'Digital Marketing');

CREATE TABLE IF NOT EXISTS courses_has_customers
    (id INT PRIMARY KEY AUTO_INCREMENT,
    id_course INT NOT NULL,
    id_customer INT NOT NULL);



