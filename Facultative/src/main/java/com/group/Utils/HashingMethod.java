package com.group.Utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingMethod {
    private static final Logger logger = LogManager.getLogger("HashPass");

    private MessageDigest md5;

    public HashingMethod() {
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            logger.info("NoSuchAlgorithmException");
            logger.error("NoSuchAlgorithmException", e);
        }
    }

    public String hashPassword(String password) {
        md5.update(password.getBytes());
        byte[] digest = md5.digest();
        StringBuffer strBuff = new StringBuffer();
        for (byte b : digest) {
            strBuff.append(String.format("%02x", b & 0xff));
        }
        return strBuff.toString();
    }
}

