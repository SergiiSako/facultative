package com.group.Utils;

import com.group.entities.Course;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sortable {
    public static List<Course> getCoursesSortedByNameUp(List<Course> courses) {
        Collections.sort(courses);
        return courses;
    }

    public static List<Course> getCoursesSortedByNameDown(List<Course> courses) {
        Collections.sort(courses,new Comparator<Course>() {
            public int compare(Course c1, Course c2) {
                return c2.getName().compareTo(c1.getName());
            }
        });
        return courses;
    }

    public static List<Course> getCoursesSortedByDuration(List<Course> courses) {
        Collections.sort(courses,new Comparator<Course>() {
            public int compare(Course c1, Course c2) {
                return Integer.compare(c1.getDuration(), c2.getDuration());
            }
        });
        return courses;
    }
    public static List<Course> getCoursesSortedByTheme(List<Course> courses) {
        Collections.sort(courses,new Comparator<Course>() {
            public int compare(Course c1, Course c2) {
                return Integer.compare(c1.getId_theme(), c2.getId_theme());
            }
        });
        return courses;
    }
    public static List<Course> getCoursesSortedByState(List<Course> courses) {
        Collections.sort(courses,new Comparator<Course>() {
            public int compare(Course c1, Course c2) {
                return c1.getState().compareTo(c2.getState());
            }
        });
        return courses;
    }

}

