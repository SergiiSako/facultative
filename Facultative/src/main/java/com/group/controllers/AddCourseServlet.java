package com.group.controllers;

import com.group.db.DAO;
import com.group.db.DBException;
import com.group.db.courses.CoursesDAO;
import com.group.entities.Course;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/view/admin/addCourse")
public class AddCourseServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String idLector = req.getParameter("idLector");
        String state = req.getParameter("state");
        Integer id_theme = Integer.parseInt(req.getParameter("themes_id_theme"));
        String start = req.getParameter("start");
        Integer duration = Integer.parseInt(req.getParameter("duration"));
        String description = req.getParameter("description");

        Course course = new Course(1, name, idLector, state, id_theme, start, duration, description);
        DAO dao = new CoursesDAO();
        try {
            try {
                dao.insert(course);
                resp.sendRedirect(req.getContextPath() + "/Courses");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (DBException e) {
            e.printStackTrace();
            req.setAttribute("errorMessage", e.getMessage());
            req.getRequestDispatcher("error.jsp").forward(req, resp);
        }
    }
}
