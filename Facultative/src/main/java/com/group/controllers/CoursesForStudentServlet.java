package com.group.controllers;

import com.group.Utils.Sortable;
import com.group.db.DBException;
import com.group.db.courses.CoursesDAO;
import com.group.entities.Course;
import com.group.entities.Customer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/CoursesForStudent")
public class CoursesForStudentServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CoursesDAO coursesDAO = new CoursesDAO();
        HttpSession session = req.getSession();
        Customer current = (Customer) session.getAttribute("cus");
        String idCourseForAdd = req.getParameter("idCourseForAdd");
        String idCourseForDelete = req.getParameter("idCourseForDelete");
        if ( idCourseForAdd != null) {
            try {
                coursesDAO.addCourseForCustomer(Integer.parseInt(idCourseForAdd), current.getIdCustomer());
            } catch (DBException e) {
                e.printStackTrace();
            }
        } else if (idCourseForDelete != null){
            try {
                coursesDAO.deleteCourseForStudent(Integer.parseInt(idCourseForDelete), current.getIdCustomer());
            } catch (DBException e) {
                e.printStackTrace();
            }
        }
        resp.sendRedirect("/Facultative/CoursesForStudent");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CoursesDAO coursesDAO = new CoursesDAO();
        HttpSession session = req.getSession();
        Customer current = (Customer) session.getAttribute("cus");
        List<Course> courses = coursesDAO.getAllCoursesForStudent(current.getIdCustomer());
        req.setAttribute("CoursesForStudent", Sortable.getCoursesSortedByState(courses));
        req.getRequestDispatcher("/view/student/studentProfile.jsp").forward(req, resp);
    }
}
