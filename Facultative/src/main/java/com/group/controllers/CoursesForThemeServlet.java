package com.group.controllers;

import com.group.db.courses.CoursesDAO;
import com.group.entities.Course;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/CoursesForTheme")
public class CoursesForThemeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        CoursesDAO coursesDAO = new CoursesDAO();
        List<Course> courses = coursesDAO.getAllCoursesForTheme(Integer.parseInt(id));
        req.setAttribute("listCourse", courses);
        getServletContext().getRequestDispatcher("/view/CoursesForTheme.jsp").forward(req, resp);

    }
}
