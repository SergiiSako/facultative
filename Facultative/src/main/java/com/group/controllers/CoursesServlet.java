package com.group.controllers;

import com.group.Utils.Sortable;
import com.group.db.DBException;
import com.group.db.courses.CoursesDAO;
import com.group.entities.Course;
import com.group.entities.Customer;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet("/Courses")
public class CoursesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("delete") != null) {
            String action = request.getParameter("delete");
            CoursesDAO coursesDAO = new CoursesDAO();
            try {
                coursesDAO.delete(Integer.parseInt(action));
            } catch (DBException e) {
                e.printStackTrace();
            }
        }
        if (request.getParameter("edit") != null) {
            CoursesDAO coursesDAO = new CoursesDAO();
            Course course = coursesDAO.getById(Integer.parseInt(request.getParameter("edit")));
            coursesDAO.update(course);
        }
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        List<Course> courses = (List<Course>) request.getAttribute("listCourse");
        if (courses == null) {
            CoursesDAO coursesDAO = new CoursesDAO();
            courses = coursesDAO.getAll();
            request.setAttribute("listCourse", courses);
        }
        List<Course> sort = null;
        String operation = request.getParameter("op");
        if (operation == null) {
            sort = courses;
        } else if (operation.equals("1")) { //sort by name
            sort = Sortable.getCoursesSortedByNameUp(courses);
        } else if (operation.equals("2")) { //sort
            sort = Sortable.getCoursesSortedByNameDown(courses);
        } else if (operation.equals("3")) { //sort
            sort = Sortable.getCoursesSortedByDuration(courses);
        }
        request.setAttribute("sortCourses", sort);
        HttpSession session = request.getSession();
        Customer current = (Customer) session.getAttribute("cus");
        if (current == null || "LECTOR".equals(current.getRole())) {
            request.getRequestDispatcher("/view/courses.jsp").forward(request, response);
        } else if ("ADMIN".equals(current.getRole())) {
            request.getRequestDispatcher("/view/admin/adminCourses.jsp").forward(request, response);
        } else if ("STUDENT".equals(current.getRole())) {
            request.getRequestDispatcher("/view/student/studentCourses.jsp").forward(request, response);
        }
    }
}
