package com.group.controllers;

import com.group.db.customers.CustomerDAO;
import com.group.entities.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/Customers")
public class CustomersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("id") != null) {
            CustomerDAO customerDAO = new CustomerDAO();
            Customer cus = customerDAO.getById(Integer.parseInt(request.getParameter("id")));
            customerDAO.update(cus);
        }
        CustomerDAO customerDAO = new CustomerDAO();
        List<Customer> customers = customerDAO.getAll();
        request.setAttribute("listCustomers", customers);
        request.getRequestDispatcher("/view/admin/customers.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
