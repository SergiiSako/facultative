package com.group.controllers;

import com.group.db.DBManager;
import com.group.entities.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/Journal")
public class JournalServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idCourse = req.getParameter("idCourse");
        if (idCourse == null){
            HttpSession session = req.getSession();
            idCourse = (String) session.getAttribute("idCourse");
        }
        String nameCourse = req.getParameter("nameCourse");
        if (nameCourse == null){
            HttpSession session = req.getSession();
            nameCourse = (String) session.getAttribute("nameCourse");
        }
        List<Customer> customers = getCustomersForJournal(idCourse);
        req.setAttribute("customers", customers);
        req.setAttribute("idCourse", idCourse);
        req.setAttribute("nameCourse", nameCourse);
        req.getRequestDispatcher("/view/lector/journal.jsp").forward(req, resp);
    }

    private List<Customer> getCustomersForJournal(String idCourse) {
        String SQL = "SELECT * from journals \n" +
                "join customers on customers.id_customer = journals.id_customer\n" +
                "where journals.id_cours = ?";
        List<Customer> customers = new ArrayList<>();
        Customer customer;
        Connection connection = null;
        if (idCourse != null) {
            try {
                connection = DBManager.getConnection();
                PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, idCourse);
                ps.execute();
                ResultSet resultSet = ps.getResultSet();
                while (resultSet.next()) {
                    customer = new Customer(resultSet.getInt("id_customer"),
                            resultSet.getString("name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getString("email"),
                            resultSet.getString("role"),
                            resultSet.getString("state"));
                    customer.setMarks(Integer.parseInt(idCourse), resultSet.getInt("mark"));
                    customers.add(customer);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }
}
