package com.group.controllers;

import com.group.Utils.HashingMethod;
import com.group.Utils.Sortable;
import com.group.db.DBManager;
import com.group.db.courses.CoursesDAO;
import com.group.db.customers.CustomerDAO;
import com.group.entities.Course;
import com.group.entities.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/view/login")
public class LoginServlet extends HttpServlet {
    private CustomerDAO customerDAO;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        customerDAO = new CustomerDAO();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Customer customer = null;
        if (!(login.equals("null") || login.equals(""))) {
            customer = customerDAO.getByLogin(login);
            if (customer != null) {
                HashingMethod hashingMethod = new HashingMethod();
                String newHashPass = hashingMethod.hashPassword(password);
                if (customer.getPassword().equals(newHashPass)) {
                    session.setAttribute("cus", customer);
                }
            }
        }
        if (customer.getRole().equals("ADMIN")) {
            request.getRequestDispatcher("admin/adminProfile.jsp").forward(request, response);
        } else if (customer.getRole().equals("LECTOR")) {
            CoursesDAO coursesDAO = new CoursesDAO();
            List<Course> courses = coursesDAO.getAllCoursesForLector(customer.getIdCustomer());
            session = request.getSession(false);
            session.setAttribute("CoursesForLector", Sortable.getCoursesSortedByState(courses));
            request.getRequestDispatcher("lector/lectorProfile.jsp").forward(request, response);
        } else {
            CoursesDAO coursesDAO = new CoursesDAO();
            CustomerDAO customerDAO = new CustomerDAO();
            List<Course> courses = coursesDAO.getAllCoursesForStudent(customer.getIdCustomer());
            session = request.getSession(false);
            session.setAttribute("Marks", customerDAO.getMarks(customer.getIdCustomer()));
            request.setAttribute("CoursesForStudent", Sortable.getCoursesSortedByState(courses));
            request.getRequestDispatcher("student/studentProfile.jsp").forward(request, response);
        }
        String message = "Incorrect data";
        request.setAttribute("errorMassage", message);
        request.getRequestDispatcher("error.jsp").forward(request, response);
    }
}
