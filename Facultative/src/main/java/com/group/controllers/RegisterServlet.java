package com.group.controllers;

import com.group.Utils.HashingMethod;
import com.group.db.customers.CustomerDAO;
import com.group.db.DAO;
import com.group.db.DBException;
import com.group.entities.Customer;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/view/registration")
public class RegisterServlet extends HttpServlet {
    private HashingMethod hasher = new HashingMethod();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastname");
        String login = req.getParameter("login");
        String password = hasher.hashPassword(req.getParameter("password"));
        String email = req.getParameter("email");
        String role = req.getParameter("role");
        if (role == null){
            role = "2";
        } else if (role.equals("LECTOR") || role.equals("3")){
            role = "3";
        }
        String state = "1";
        if (!(name.equals("null") || lastName.equals("null") || login.equals("null") || password.equals("null") || email.equals("null")
                || name.equals("") || lastName.equals("") || login.equals("") || password.equals("") || email.equals(""))) {
            Customer n = new Customer(1, name, lastName, login, password, email, role, state);
            DAO dao = new CustomerDAO();
            try {
                try {
                    dao.insert(n);
                    if (role.equals("3")){
                        resp.sendRedirect(req.getContextPath() + "/Customers");
                        return;
                    } else {
                        String success = "Success registration";
                        req.setAttribute( "message", success);
                        req.getRequestDispatcher("login.jsp").forward(req, resp);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (DBException e) {
                e.printStackTrace();
                req.setAttribute("errorMessage", e.getMessage());
                req.getRequestDispatcher("error.jsp").forward(req, resp);
            }
        }
        String message = "Incorrect data";
        req.setAttribute("errorMassage", message);
        req.getRequestDispatcher("error.jsp").forward(req, resp);
    }
}