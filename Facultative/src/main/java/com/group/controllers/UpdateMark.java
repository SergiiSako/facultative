package com.group.controllers;
import com.group.db.DBManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/UpdateMark")
public class UpdateMark extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String mark = req.getParameter("mark");
        String idCustomer = req.getParameter("idCustomer");
        String idCourse = req.getParameter("idCourse");
        String nameCourse = req.getParameter("nameCourse");
        if (mark != null) {
            updateMark(mark, idCustomer, idCourse);
        }
        HttpSession session = req.getSession();
        session.setAttribute("idCourse", idCourse);
        session.setAttribute("nameCourse", nameCourse);
        resp.sendRedirect("/Facultative/Journal");
    }

    private void updateMark(String mark, String idCustomer, String idCourse) {
        String SQL = "update journals set mark = ? where id_customer = ? and id_cours = ?";
        Connection connection = null;
        try {
            connection = DBManager.getConnection();
            PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, mark);
            ps.setString(2, idCustomer);
            ps.setString(3, idCourse);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
