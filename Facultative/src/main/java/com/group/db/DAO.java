package com.group.db;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
    void insert(T t) throws DBException, SQLException;
    T getById(int id);
    T getByLogin(String login);
    List<T> getAll();
    void update(T t);
}
