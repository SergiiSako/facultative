package com.group.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBManager {
    private static final Logger LOG = LogManager.getLogger();
    private static DataSource ds;

    static {
        try {
            Context initCtx = new InitialContext();
            Context envContext = (Context) initCtx.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/facultative");
            LOG.info("DataSource successful");
        } catch (NamingException ex) {
            LOG.error("Cannot find the data source");
            throw new IllegalStateException("Cannot init DBManager", ex);
        }
    }

    private DBManager() {
    }

    ;

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public static void close(AutoCloseable ac){
        if (ac != null){
                try {
                    ac.close();
                } catch (Exception e) {
                    LOG.error("Cannot close ac");
                }
        }
    }
}

