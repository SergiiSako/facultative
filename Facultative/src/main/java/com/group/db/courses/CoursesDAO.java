package com.group.db.courses;

import com.group.db.DAO;
import com.group.db.DBException;
import com.group.db.DBManager;
import com.group.entities.Course;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CoursesDAO implements DAO<Course> {
    private static final Logger logger = LogManager.getLogger(CoursesDAO.class);

    @Override
    public void insert(Course course) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        int index = 0;
        final String INSERT_COURSE = "INSERT INTO courses VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
        try {
            connection = DBManager.getConnection();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(INSERT_COURSE, Statement.RETURN_GENERATED_KEYS);
            ps.setString(++index, course.getName());
            ps.setString(++index, course.getId_lector());
            ps.setString(++index, course.getState());
            ps.setInt(++index, course.getId_theme());
            ps.setString(++index, course.getStart());
            ps.setInt(++index, course.getDuration());
            ps.setString(++index, course.getDescription());
            if (ps.executeUpdate() > 0) {
                resultSet = ps.getGeneratedKeys();
                if (resultSet.next()) {
                    course.setId(resultSet.getInt(1));
                }
            }
            connection.commit();
        } catch (SQLException e) {
            logger.error("SQL exception", e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("SQL exception", e);
            }
            throw new DBException("Cannot insert course", e);
        } finally {
            DBManager.close(resultSet);
            DBManager.close(ps);
            DBManager.close(connection);
        }
    }


    @Override
    public Course getById(int id) {
        Course course = null;
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses WHERE id_course = ?")) {
                    connection.setAutoCommit(false);
                    statement.setInt(1, id);
                    statement.execute();
                    ResultSet resultSet = statement.getResultSet();
                    if (resultSet.next()) {
                        course = new Course(
                                resultSet.getInt("id_course"),
                                resultSet.getString("name"),
                                resultSet.getString("id_lector"),
                                resultSet.getString("state"),
                                resultSet.getInt("themes_id_theme"),
                                resultSet.getString("start"),
                                resultSet.getInt("duration"),
                                resultSet.getString("description"));
                    }
                    resultSet.close();
                    connection.commit();
                } catch (SQLException e) {
                    logger.error("error rollback");
                    connection.rollback();
                }
            }
        } catch (SQLException ex) {
            logger.error("error getById");
        }
        return course;
    }

    @Override
    public Course getByLogin(String login) {
        Course course = null;
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM courses WHERE name = ?")) {
                    connection.setAutoCommit(false);
                    statement.setString(1, login);
                    statement.execute();
                    ResultSet resultSet = statement.getResultSet();
                    if (resultSet.next()) {
                        course = new Course(
                                resultSet.getInt("id_course"),
                                resultSet.getString("name"),
                                resultSet.getString("id_lector"),
                                resultSet.getString("state"),
                                resultSet.getInt("themes_id_theme"),
                                resultSet.getString("start"),
                                resultSet.getInt("duration"),
                                resultSet.getString("description"));
                    }
                    resultSet.close();
                    connection.commit();
                } catch (SQLException e) {
                    logger.error("er");
                    connection.rollback();
                }
            }
        } catch (SQLException ex) {
            logger.error("err");
        }
        return course;
    }

    @Override
    public List<Course> getAll() {
        List<Course> courses = new ArrayList<>();
        Course course;
        String SQL = "SELECT courses.id_course, courses.name, customers.lastname, state.name_cond, courses.themes_id_theme, courses.start, courses.duration, courses.description  FROM courses JOIN customers on courses.id_lector = customers.id_customer JOIN state on courses.state = state.id_cond";
        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SQL)) {
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    course = new Course(
                            resultSet.getInt("id_course"),
                            resultSet.getString("name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("name_cond"),
                            resultSet.getInt("themes_id_theme"),
                            resultSet.getString("start"),
                            resultSet.getInt("duration"),
                            resultSet.getString("description"));
                    courses.add(course);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }

    @Override
    public void update(Course course) {
        String state = course.getState();
        if (state.equals("1")){
            state = "2";
        } else {
            state = "1";
        }
        final String UPDATE_STATE = "UPDATE courses SET state = ? WHERE id_course = ?";
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement(UPDATE_STATE)) {
                    statement.setString(1, state);
                    statement.setInt(2, course.getId());
                    statement.execute();
                } catch (SQLException e) {
                    logger.error("er");
                }
            }
        } catch (SQLException ex) {
            logger.error("err");
        }
    }

    public List<Course> getAllCoursesForTheme(int id_theme){
        List<Course> courses = new ArrayList<>();
        Course course;
        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "SELECT courses.id_course, courses.name, customers.lastname, courses.state, courses.themes_id_theme, courses.start, courses.duration, courses.description  FROM courses JOIN customers on courses.id_lector = customers.id_customer where courses.themes_id_theme = ?")) {
                statement.setInt(1, id_theme);
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    course = new Course(
                            resultSet.getInt("id_course"),
                            resultSet.getString("name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("state"),
                            resultSet.getInt("themes_id_theme"),
                            resultSet.getString("start"),
                            resultSet.getInt("duration"),
                            resultSet.getString("description"));
                    courses.add(course);
                }
            }
        } catch (SQLException e) {
           logger.error("cannot getAllCoursesForTheme", e);
        }
        return courses;
    }

    public void delete(int id) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        final String SQL = "DELETE FROM courses WHERE id_course = ?";
        try {
            connection = DBManager.getConnection();
            ps = connection.prepareStatement(SQL);
            ps.setInt(1 , id);
            ps.execute();
        } catch (SQLException e) {
            logger.error("SQL exception", e);
            throw new DBException("Cannot delete course", e);
        } finally {
            DBManager.close(ps);
            DBManager.close(connection);
        }
    }

    public void addCourseForCustomer(int course, int customer) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        final String SQL= "INSERT INTO JOURNALS VALUES (DEFAULT, ?, ?, null)";
        try {
            connection = DBManager.getConnection();
            ps = connection.prepareStatement(SQL);
            ps.setInt(1, course);
            ps.setInt(2, customer);
            ps.execute();
        } catch (SQLException e) {
            logger.error("SQL exception", e);
            throw new DBException("Cannot insert course", e);
        } finally {
            DBManager.close(ps);
            DBManager.close(connection);
        }
    }

    public List<Course> getAllCoursesForStudent(int idStudent){
        List<Course> courses = new ArrayList<>();
        Course course;
        String SQL = "select * from journals\n" +
                "join customers on journals.id_customer=customers.id_customer\n" +
                "join courses on journals.id_cours=courses.id_course\n" +
                "join roles on customers.role=roles.id_role\n" +
                "join state on customers.state=state.id_cond\n" +
                "where journals.id_customer = ?";
        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SQL)) {
                statement.setInt(1, idStudent);
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    course = new Course(
                            resultSet.getInt("id_course"),
                            resultSet.getString("courses.name"),
                            resultSet.getString("courses.id_lector"),
                            resultSet.getString("name_cond"),
                            resultSet.getInt("themes_id_theme"),
                            resultSet.getString("start"),
                            resultSet.getInt("duration"),
                            resultSet.getString("description"));
                    courses.add(course);
                }
            }
        } catch (SQLException e) {
            logger.error("cannot getAllCoursesForStudent", e);
        }
        return courses;
    }

    public void deleteCourseForStudent(int idCourse, int idCustomer) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        final String SQL = "DELETE FROM journals WHERE id_cours = ? AND id_customer = ?";
        try {
            connection = DBManager.getConnection();
            ps = connection.prepareStatement(SQL);
            ps.setInt(1 , idCourse);
            ps.setInt(2 , idCustomer);
            ps.execute();
        } catch (SQLException e) {
            logger.error("SQL exception", e);
            throw new DBException("Cannot delete course", e);
        } finally {
            DBManager.close(ps);
            DBManager.close(connection);
        }
    }

    public List<Course> getAllCoursesForLector(int id) {
        List<Course> courses = new ArrayList<>();
        Course course;
        String SQL = "SELECT courses.id_course,courses.name,courses.id_lector,state.name_cond,courses.start,courses.themes_id_theme,courses.duration,courses.description  \n" +
                "from courses \n" +
                "join state on courses.state = state.id_cond\n" +
                "where courses.id_lector = ?";
        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(SQL)) {
                statement.setInt(1, id);
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    course = new Course(
                            resultSet.getInt("id_course"),
                            resultSet.getString("name"),
                            resultSet.getString("id_lector"),
                            resultSet.getString("state.name_cond"),
                            resultSet.getInt("themes_id_theme"),
                            resultSet.getString("start"),
                            resultSet.getInt("duration"),
                            resultSet.getString("description"));
                    courses.add(course);
                }
            }
        } catch (SQLException e) {
            logger.error("cannot getAllCoursesForStudent", e);
        }
        return courses;
    }
}

