package com.group.db.customers;

import com.group.db.DAO;
import com.group.db.DBException;
import com.group.db.DBManager;
import com.group.entities.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerDAO implements DAO<Customer> {
    private static final Logger logger = LogManager.getLogger(CustomerDAO.class);

    @Override
    public void insert(Customer customer) throws DBException {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        int index = 0;
        final String INSERT_CUSTOMER = "INSERT INTO customers VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)";
        try {
            connection = DBManager.getConnection();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(INSERT_CUSTOMER, Statement.RETURN_GENERATED_KEYS);
            ps.setString(++index, customer.getName());
            ps.setString(++index, customer.getLastName());
            ps.setString(++index, customer.getLogin());
            ps.setString(++index, customer.getPassword());
            ps.setString(++index, customer.getEmail());
            ps.setString(++index, customer.getRole());
            ps.setString(++index, customer.getState());
            if (ps.executeUpdate() > 0) {
                resultSet = ps.getGeneratedKeys();
                if (resultSet.next()) {
                    customer.setIdCustomer(resultSet.getInt(1));
                }
            }
            connection.commit();
        } catch (SQLException e) {
            logger.error("SQL exception", e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("SQL exception", e);
            }
            throw new DBException("Cannot insert customer", e);
        } finally {
            DBManager.close(resultSet);
            DBManager.close(ps);
            DBManager.close(connection);
        }
    }

    @Override
    public Customer getById(int id) {
        Customer customer = null;
        final String sGetById = "SELECT * FROM customers WHERE id_customer = ?";
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement(sGetById)) {
                    statement.setInt(1, id);
                    statement.execute();
                    ResultSet resultSet = statement.getResultSet();
                    if (resultSet.next()) {
                        customer = new Customer(resultSet.getInt("id_customer"),
                                resultSet.getString("name"),
                                resultSet.getString("lastname"),
                                resultSet.getString("login"),
                                resultSet.getString("password"),
                                resultSet.getString("email"),
                                resultSet.getString("role"),
                                resultSet.getString("state"));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    logger.error("er");
                }
            }
        } catch (SQLException ex) {
            logger.error("err");
        }
        return customer;
    }

    @Override
    public Customer getByLogin(String login) {
        Customer customer = null;
        final String sGetByLogin = "SELECT customers.id_customer, customers.name, customers.lastname, customers.login, customers.email, customers.password, roles.name_role, state.name_cond\n" +
                "FROM customers\n" +
                "JOIN roles on  customers.role = roles.id_role\n" +
                "JOIN state on customers.state = state.id_cond\n" +
                " WHERE login = ?";
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement(sGetByLogin)) {
                    statement.setString(1, login);
                    statement.execute();
                    ResultSet resultSet = statement.getResultSet();
                    if (resultSet.next()) {
                        customer = new Customer(resultSet.getInt("id_customer"),
                                resultSet.getString("name"),
                                resultSet.getString("lastname"),
                                resultSet.getString("login"),
                                resultSet.getString("password"),
                                resultSet.getString("email"),
                                resultSet.getString("name_role"),
                                resultSet.getString("name_cond"));
                    }
                    resultSet.close();
                } catch (SQLException e) {
                    logger.error("er");
                }
            }
        } catch (SQLException ex) {
            logger.error("err");
        }
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        final String sGetAll = "SELECT customers.id_customer, customers.name, customers.lastname, customers.login, customers.email, customers.password, roles.name_role, state.name_cond\n" +
                "FROM customers\n" +
                "JOIN roles on  customers.role = roles.id_role\n" +
                "JOIN state on customers.state = state.id_cond";
        List<Customer> customers = new ArrayList<>();
        Customer customer;
        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(sGetAll)) {
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    customer = new Customer(resultSet.getInt("id_customer"),
                            resultSet.getString("name"),
                            resultSet.getString("lastname"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getString("email"),
                            resultSet.getString("name_role"),
                            resultSet.getString("name_cond"));
                    customers.add(customer);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public void update(Customer customer) {
    String state = customer.getState();
    if (state.equals("1")){
        state = "2";
    } else {
        state = "1";
    }
    final String UPDATE_STATE = "UPDATE customers SET state = ? WHERE id_customer = ?";
        try (Connection connection = DBManager.getConnection()) {
            if (connection != null) {
                try (PreparedStatement statement = connection.prepareStatement(UPDATE_STATE)) {
                    statement.setString(1, state);
                    statement.setInt(2, customer.getIdCustomer());
                    statement.execute();
                } catch (SQLException e) {
                    logger.error("er");
                }
            }
        } catch (SQLException ex) {
            logger.error("err");
        }
    }

    public Map<Integer, Integer> getMarks(int idCustomer) {
        Map<Integer, Integer> marks = new HashMap<>();
        String SQL = "SELECT * from journals \n" +
                "where journals.id_customer = ?";
        Connection connection = null;
        try {
            connection = DBManager.getConnection();
            PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, idCustomer);
            ps.execute();
            ResultSet resultSet = ps.getResultSet();
            while (resultSet.next()) {
                marks.put(resultSet.getInt("id_cours"), resultSet.getInt("mark"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return marks;
    }
}
