package com.group.db.themes;

import com.group.db.DAO;
import com.group.db.DBException;
import com.group.db.DBManager;
import com.group.entities.Theme;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ThemesDAO implements DAO<Theme> {

    @Override
    public void insert(Theme theme) throws DBException {

    }

    @Override
    public Theme getById(int id) {
        return null;
    }

    @Override
    public Theme getByLogin(String login) {
        return null;
    }

    @Override
    public List<Theme> getAll() {
        List<Theme> themes = new ArrayList<>();
        Theme theme;

        try (Connection connection = DBManager.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM themes")) {
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    theme = new Theme(resultSet.getInt("id_theme"), resultSet.getString("name_theme"));
                    themes.add(theme);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return themes;
    }

    @Override
    public void update(Theme theme) {

    }
}