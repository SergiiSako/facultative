package com.group.entities;

import java.io.Serializable;

public class Course implements Serializable, Comparable{
    private int id;
    private String name;
    private String id_lector;
    private String state;
    private int id_theme;
    private String start;
    private int duration;
    private String description;

    public Course() {
    }

    public Course(int id, String name, String id_lector, String state, int id_theme, String start, int duration, String description) {
        this.id = id;
        this.name = name;
        this.id_lector = id_lector;
        this.state = state;
        this.id_theme = id_theme;
        this.start = start;
        this.duration = duration;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_lector() {
        return id_lector;
    }

    public void setId_lector(String id_lector) {
        this.id_lector = id_lector;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getId_theme() {
        return id_theme;
    }

    public void setId_theme(int id_theme) {
        this.id_theme = id_theme;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", id_lector=" + id_lector +
                ", state='" + state + '\'' +
                ", id_theme=" + id_theme +
                ", date='" + start + '\'' +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Course course = null;
        if (o instanceof Course){
            course = (Course) o;
        }
        return name.compareTo(course.getName());
    }
}
