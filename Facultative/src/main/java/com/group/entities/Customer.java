package com.group.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;

    private int idCustomer;
    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private String Role;
    private String State;
    private Map<Integer, Integer> marks = new HashMap<>();

    public int getMark(int idCourse) {
        return marks.get(idCourse);
    }

    public void setMarks(Integer idCourse, Integer mark) {
        marks.put(idCourse, mark);
    }


    public Customer() {
    }

    public Customer(int idCustomer, String name, String lastname, String login, String password, String email, String role, String state) {
        this.idCustomer = idCustomer;
        this.name = name;
        this.lastName = lastname;
        this.login = login;
        this.password = password;
        this.email = email;
        Role = role;
        State = state;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "idCustomer=" + idCustomer +
                ", name='" + name + '\'' +
                ", lastname='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", Role='" + Role + '\'' +
                ", State='" + State + '\'' +
                '}';
    }
}
