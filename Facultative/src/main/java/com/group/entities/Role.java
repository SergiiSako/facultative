package com.group.entities;

import java.util.Locale;

public enum Role {
    ADMIN,
    LECTOR,
    STUDENT,
    GUEST;

    @Override
    public String toString() {
        return super.toString().toUpperCase();
    }
}
