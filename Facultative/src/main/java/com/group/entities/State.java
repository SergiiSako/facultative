package com.group.entities;

public enum State {
    ACTIVE,
    BLOCKED;

    @Override
    public String toString() {
        return super.toString().toUpperCase();
    }
}
