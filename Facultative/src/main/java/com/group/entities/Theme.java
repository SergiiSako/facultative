package com.group.entities;

import java.io.Serializable;

public class Theme implements Serializable {
    private int id_theme;
    private String name_theme;

    public Theme(int id_theme, String name_theme) {
        this.id_theme = id_theme;
        this.name_theme = name_theme;
    }

    public int getId_theme() {
        return id_theme;
    }

    public void setId_theme(int id_theme) {
        this.id_theme = id_theme;
    }

    public String getName_theme() {
        return name_theme;
    }

    public void setName_theme(String name_theme) {
        this.name_theme = name_theme;
    }
}
