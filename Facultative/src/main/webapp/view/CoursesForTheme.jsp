<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>CoursesForTheme</title>
    <jsp:include page="/view/bootstrap.html"/>
</head>
<body>
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="index.jsp"> Welcome </a></li>
        <li><a href="${pageContext.request.contextPath}/view/login.jsp"> Login </a></li>
        <li><a href="${pageContext.request.contextPath}/view/registration.jsp"> Registration </a></li>

    </ul>
</nav>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Course</th>
            <th>Lector</th>
            <th>State</th>
            <th>Start</th>
            <th>Duration/Month</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="course" items="${listCourse}">
        <tr>
            <td>${course.getId()}</td>
            <td>${course.getName()}</td>
            <td>${course.getId_lector()}</td>
            <td>${course.getState()}</td>
            <td>${course.getStart()}</td>
            <td>${course.getDuration()}</td>
            <td>${course.getDescription()}</td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
