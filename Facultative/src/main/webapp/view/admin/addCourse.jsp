<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>AddCourse</title>
    <jsp:include page="/view/bootstrap.html"/>
</head>
<body>
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="../../index.jsp"> Welcome</a></li>
        <li><a href="../registration.jsp"> Registration</a></li>
        <li><a href="../login.jsp"> Login</a></li>
        <li><a href="${pageContext.request.contextPath}/Courses" class="nav-link"> Courses </a></li>
        <li><a href="<%=request.getContextPath()%>/Customers" class="nav-link">Customers</a></li>
    </ul>
</nav>
<form action="addCourse" method="POST" role="form">
    <c:if test="${sessionScope.failed == 1}">
        <div class="form-group">
            <h3>failed</h3>
        </div>
    </c:if>

    <div class="container">
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="idLector">idLector:</label>
            <input type="text" class="form-control" id="idLector" name="idLector">
        </div>
        <div class="form-group">
            <label for="state">State:</label>
            <input type="text" class="form-control" id="state" name="state">
        </div>
        <div class="form-group">
            <label for="themes_id_theme">themes_id_theme:</label>
            <input type="text" class="form-control" id="themes_id_theme" name="themes_id_theme">
        </div>
        <div class="form-group">
            <label for="start">Start:</label>
            <input type="date" class="form-control" id="start" name="start">
        </div>
        <div class="form-group">
            <label for="duration">Duration:</label>
            <input type="text" class="form-control" id="duration" name="duration">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
</form>
</body>
</html>