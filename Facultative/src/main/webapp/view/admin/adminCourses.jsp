<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> All Courses </title>
    <jsp:include page="/view/bootstrap.html"/>
</head>
<body>
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/index.jsp"> Welcome </a></li>
        <li><a href="${pageContext.request.contextPath}/view/login.jsp"> Login </a></li>
        <li><a href="${pageContext.request.contextPath}/view/registration.jsp"> Registration </a></li>
        <li><a href="<%=request.getContextPath()%>/Customers" class="nav-link">Customers</a></li>

    </ul>
</nav>
<form action = "Courses" method="get">
    <select name="op">
        <option value="1"> Sort By Name (A - Z)
        <option value="2"> Sort By Name (Z - A)
        <option value="3"> Sort By Duration
    </select>
    <input type ="submit" value="Sort">
</form>

<c:if test="${cus.getRole() == 'ADMIN' }">
<div class="container text-left">
    <a href="<%=request.getContextPath()%>/view/admin/addCourse.jsp" class="btn btn-success">Add
        New Course</a>
</div>
</c:if>
    <br>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Course</th>
            <th>Lector</th>
            <th>State</th>
            <th>Start</th>
            <th>Duration/Month</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <form action="Courses" method="get">
        <c:forEach var="course" items="${sortCourses}">
            <tr>
                <td>${course.getId()}</td>
                <td>${course.getName()}</td>
                <td>${course.getId_lector()}</td>
                <td>${course.getState()}</td>
                <td>${course.getStart()}</td>
                <td>${course.getDuration()}</td>
                <td>${course.getDescription()}</td>
                <td>
                    <button name="delete" value="${course.getId()}"> Delete </button>
                    <button name="edit" value="${course.getId()}"> Edit </button>
                </td>
            </tr>
        </c:forEach>
        </form>
        </tbody>
    </table>
</div>
</body>
</html>


