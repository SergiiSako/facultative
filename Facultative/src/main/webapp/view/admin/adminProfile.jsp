<%@ page import="com.group.entities.Customer" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/Courses"
                   class="nav-link">Courses</a></li>
            <li><a href="<%=request.getContextPath()%>/Customers"
                   class="nav-link">Customers</a></li>
        </ul>
    </nav>
</header>
<br>
    <div class="container">
        <h3 class="text-center">Profile</h3>
        <hr>
        <br>
        <p>Name: ${cus.getName()}</p>
        <p>LastName: ${cus.getLastName()}</p>
        <p>Login: ${cus.getLogin()}</p>
        <p>Email: ${cus.getEmail()}</p>
        <p>Role: ${cus.getRole()}</p>
    </div>
</div>

</body>
</html>