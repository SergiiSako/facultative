<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Customers</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <div>
            <a href="<%=request.getContextPath()%>/view/admin/adminProfile.jsp" class="navbar-brand">
                Profile </a>
        </div>

        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/Courses"
                   class="nav-link">Courses</a></li>
        </ul>
    </nav>
</header>
<br>

<div class="row">
    <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

    <div class="container">
        <h3 class="text-center">List of customers</h3>
        <hr>
        <div class="container text-left">
            <a href="<%=request.getContextPath()%>/view/registration.jsp" class="btn btn-success">Add
                New Lector</a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>LastName</th>
                <th>Login</th>
                <th>Email</th>
                <th>Role</th>
                <th>State</th>
            </tr>
            </thead>
            <tbody>
            <form action="Customers" method="get">
                <c:forEach var="customer" items="${listCustomers}">
                    <tr>
                        <td>${customer.getIdCustomer()}</td>
                        <td>${customer.getName()}</td>
                        <td>${customer.getLastName()}</td>
                        <td>${customer.getLogin()}</td>
                        <td>${customer.getEmail()}</td>
                        <td>${customer.getRole()}</td>
                        <td><button name="id" value="${customer.getIdCustomer()}"> ${customer.getState()} </button> </td>
                    </tr>
                </c:forEach>
            </form>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>