<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Journal</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <ul class="navbar-nav">
            <li><a href="${pageContext.request.contextPath}/view/login.jsp" class="nav-link"> Login </a></li>
            <li><a href="${pageContext.request.contextPath}/view/lector/lectorProfile.jsp" class="nav-link">Profile</a></li>
        </ul>
    </nav>
</header>
<br>
<h3 class="text-center">Journal ${nameCourse}</h3>
<p> Count Student = ${customers.size()} </p>
<hr>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>LastName</th>
            <th>Email</th>
            <th>Marks</th>
            <th></th>
        </tr>
        </thead>
            <tbody>
            <c:forEach var="cus" items="${customers}">
            <tr>
                <td>${cus.getName()}</td>
                <td>${cus.getLastName()}</td>
                <td>${cus.getEmail()}</td>
                <td>${cus.getMark(idCourse)}</td>
                <td>
        <form action="UpdateMark" method="post">
                    <input type="number" id="mark" name="mark" min="2" max="5">
                    <input type="hidden" name="idCustomer" value="${cus.getIdCustomer()}">
                    <input type="hidden" name="idCourse" value="${idCourse}">
                    <input type="hidden" name="nameCourse" value="${nameCourse}">
                    <input type="submit" value="Evaluate">
        </form>
                </td>
                </tr>
            </c:forEach>
            </tbody>
    </table>
</div>
</body>
</html>