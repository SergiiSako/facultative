<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Lector</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <ul class="navbar-nav">
            <li><a href="${pageContext.request.contextPath}/view/login.jsp" class="nav-link"> Login </a></li>
            <li><a href="${pageContext.request.contextPath}/Courses" class="nav-link">Courses</a></li>
        </ul>
    </nav>
</header>
<br>
    <div class="container">
        <h3 class="text-center">Profile</h3>
        <hr>
        <br>
        <p>Name: ${cus.getName()}</p>
        <p>LastName: ${cus.getLastName()}</p>
        <p>Login: ${cus.getLogin()}</p>
        <p>Email: ${cus.getEmail()}</p>
        <p>Role: ${cus.getRole()}</p>
    </div>
</div>
<hr>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>idCourse</th>
            <th>Course</th>
            <th>State</th>
            <th>Start</th>
            <th>Duration/Month</th>
            <th>Description</th>
            <th>Journals</th>

        </tr>
        </thead>
        <tbody>
            <c:forEach var="course" items="${CoursesForLector}">
            <tr>
                <td>${course.getId()}</td>
                <td>${course.getName()}</td>
                <td>${course.getState()}</td>
                <td>${course.getStart()}</td>
                <td>${course.getDuration()}</td>
                <td>${course.getDescription()}</td>
                <td><a href="${pageContext.request.contextPath}/Journal?idCourse=${course.getId()}&nameCourse=${course.getName()}"> Journal </a></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>