<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <jsp:include page="/view/bootstrap.html" />
</head>
<body>
<nav class="navbar navbar-default" >
    <ul class="nav navbar-nav">
        <li><a href="../index.jsp"> Welcome </a></li>
        <li class="active"><a href="login.jsp"> Login </a> </li>
        <li><a href="registration.jsp"> Registration </a> </li>
    </ul>
</nav>
<form action="login" method="POST" role="form">
    <c:if test="${sessionScope.failed == 1}">
        <div class="form-group">
            <h3>Login failed</h3>
        </div>
    </c:if>
    <c:if test="${message != null}">
        <h1 class="display-2">${message}</h1>
    </c:if>
    <div class="container">
    <div class="form-group">
        <label for="login">Login:</label>
        <input type="text" class="form-control" id="login" name="login">
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
    </div>
</form>
</body>
</html>