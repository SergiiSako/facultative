<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <jsp:include page="/view/bootstrap.html"/>
</head>
<body>
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="../index.jsp"> Welcome</a></li>
        <li class="active"><a href="registration.jsp"> Registration</a></li>
        <li><a href="login.jsp"> Login</a></li>
    </ul>
</nav>
<form action="registration" method="POST" role="form">
    <c:if test="${sessionScope.failed == 1}">
        <div class="form-group">
            <h3>registration failed</h3>
        </div>
    </c:if>

    <div class="container">

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="lastname">LastName:</label>
            <input type="text" class="form-control" id="lastname" name="lastname">
        </div>

        <div class="form-group">
            <label for="login">Login:</label>
            <input type="text" class="form-control" id="login" name="login">
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>


        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>

        <c:if test="${cus.getRole() == 'ADMIN' }">
            <div class="form-group">
                <label for="role">Role:</label>
                <input type="text" class="form-control" id="role" name="role">
            </div>
        </c:if>
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
</form>
</body>
</html>