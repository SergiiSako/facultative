<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> Student Courses </title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <ul class="navbar-nav">
            <li><a href="${pageContext.request.contextPath}/view/login.jsp" class="nav-link"> Login </a></li>
            <li><a href="${pageContext.request.contextPath}/CoursesForStudent" class="nav-link"> Profile </a></li>
        </ul>
    </nav>
</header>
<br>
<form action="Courses" method="get">
    <select name="op">
        <option value="1"> Sort By Name (A - Z)
        <option value="2"> Sort By Name (Z - A)
        <option value="3"> Sort By Duration
    </select>
    <input type="submit" value="Sort">
</form>

<br>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Course</th>
            <th>Lector</th>
            <th>State</th>
            <th>Start</th>
            <th>Duration/Month</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <form action="CoursesForStudent" method="post">
            <c:forEach var="course" items="${sortCourses}">
                <tr>
                    <td>${course.getId()}</td>
                    <td>${course.getName()}</td>
                    <td>${course.getId_lector()}</td>
                    <td>${course.getState()}</td>
                    <td>${course.getStart()}</td>
                    <td>${course.getDuration()}</td>
                    <td>${course.getDescription()}</td>
                    <c:if test="${course.getState() == 'ACTIVE'}">
                    <td><button name="idCourseForAdd" value="${course.getId()}"> Add </button></td>
                    </c:if>
                </tr>
            </c:forEach>
        </form>
        </tbody>
    </table>
</div>
</body>
</html>


