<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<sql:query var="rs" dataSource="jdbc/facultative">
    select id_theme, name_theme from themes order by name_theme asc
</sql:query>
<html>
    <title> Home </title>
    <jsp:include page="/view/bootstrap.html" />
<body>
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li class="active"><a href="index.jsp"> Welcome </a></li>
        <li><a href="view/login.jsp"> Login </a></li>
        <li><a href="view/registration.jsp"> Registration </a></li>
    </ul>
</nav>
<div class="page-header">
    <h1>Themes ours courses</h1>
</div>

<div class="container">
<c:forEach var="row" items="${rs.rows}">
    <p> <a href="CoursesForTheme?id=${row.id_theme}"> ${row.name_theme} </a> </p>
</c:forEach>
</div>
<hr>
    <h2> <a href="Courses"> Courses </a> </h2>
</div>
</body>
</html>